% Ahmed Dallal
% August 2015
%%  Ledalab data: ivn07_16_matlab

clear all, close all

params.LMtype = 'LocalMin'; % extraction prameters
params.LMdefn = 'strict';

load('ivn07_16_matlab.mat');


time = data.time;
timeOffset=time(1);
timeNorm=time-time(1);

eda_full = data.conductance;

load('0p35HzLPFilter.mat')

eda_filt=conv(Num, eda_full);
eda_filt=eda_filt((length(Num)/2)+1: end-length(Num)/2+1);
figure, plot(timeNorm, eda_filt), hold on
xlabel('Time (Seconds)'); ylabel('Micro Siemens');

[indExt,valExt] = findLocalExtrema(eda_filt,params);
t_extrim = timeNorm(indExt);
stem(timeNorm(indExt),valExt)

tonicData_est1 = interp1(t_extrim,valExt,timeNorm); 
figure, plot(timeNorm, tonicData_est1)
hold on, plot(timeNorm, eda_filt, 'k')
plot(timeNorm, eda_filt - tonicData_est1, 'r')
legend('estimation of tonic level ', 'original data', 'estimated phasic component')
title('Interpolation of local minima (linear)')
xlabel('Time (Seconds)'); ylabel('Micro Siemens');

% Time windowing
SCR_TimeLength = 3; % (Seconds) this can be edited fro different lenthes of SCR
% 
[t_extrim_filtered, idx] = Time_Threshold(t_extrim,SCR_TimeLength);
tonicData_est2 = interp1(t_extrim_filtered,valExt(idx),timeNorm); 
figure, plot(timeNorm, tonicData_est2)
hold on, plot(timeNorm, eda_filt, 'k')
plot(timeNorm, eda_filt - tonicData_est2, 'r')
legend('estimation of tonic level ', 'original data', 'estimated phasic component')
title('Interpolation of local minima (linear) + minima time thresholding')
xlabel('Time (Seconds)'); ylabel('Micro Siemens');
% compare to ledalab
leda_phasic = analysis.phasicData;
leda_tonic = analysis.tonicData;
figure('Name','compare to leda')
subplot 211, plot(timeNorm,tonicData_est2,'r-',timeNorm,leda_tonic,'b-.',timeNorm,eda_filt,'k.')
title('tonic component')
legend('ours','ledalab','original data')
subplot 212, plot(timeNorm, eda_filt - tonicData_est2,'r-',timeNorm, leda_phasic,'b-.')
title('phasic component')
legend('ours','ledalab')





% Ahmed Dallal
% last updated: Aug 27, 2015 
