% This script count the number of SCRs detected by ledalab analysis in the
% _scllist file
% select the root folder that holds your patients data when prompted 
% written by: Ahmed Dallal
% Oct 2015
close all, clear all

% get root folder
p = uigetdir;
% loop on each patient
for patient = 1:5
    path = strcat(p,{'\patient '},num2str(patient),'\data_analyzed');
    path = path{1};
    cd(path)
    % get number of files
    d = dir;
    d([1 2]) = [];
    file_names = extractfield(d,'name');
    % extract the scr list files
    scrList_id = cellfun(@isempty,strfind(file_names,'_scrlist'));
    scrList_files = file_names(scrList_id == 0);    % chose the NOT empty indeces
    % count the number of scrs in each file
    for i = 1:length(scrList_files)
        load(scrList_files{i});
        N_SCRs = length(scrList.DDA.onset);
        patient_file = strsplit(scrList_files{i},'_scrlist');
        patient_file = strcat(patient_file{1},'.mat');
        % save the number of scrs with the data
        save(patient_file,'N_SCRs','-append')
    end
end


% copyrights: Ahmed Dallal