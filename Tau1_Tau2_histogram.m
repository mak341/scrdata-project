% this script will extract the Tau 1 and Tau 2 values for each scr and then
% concatenate them into a signal vector so that a histogram of the values
% can be made. 

clear all;
clc;
% get root folder 
p = uigetdir; 
%create initial t1 and t2
t1 = [];
t2 = [];
% Loop through each patient
for patient = 5:5
    path = strcat(p,{'\patient '},num2str(patient),'\data_analyzed');
    path = path{1};
    cd(path)
    % get # of files for next loop
    d = dir('*_scrlist.mat');
    files = extractfield(d,'name');
    
    % loop through files
    for i = 1:length(files)
        date_file = strsplit(files{i},'_scrlist.mat');
        date_file = strcat(date_file{1},'.mat');
        load(date_file)
        %check in there are SCRs in file 
        if N_SCRs == 0
            t1 = t1;
            t2 = t2;
        else
            %extract t1 and t2
            x = tau_est(:,2);
            y = tau_est(:,3);
            
            t1 = vertcat(t1,x);
            t2 = vertcat(t2,y);
        end
    end
    
end

% create histogram
% edge = [0 .05:.05:.45 .5];
figure,histogram(t1)
title('Patient 6 Tau1 histogram')
figure,histogram(t2)
title('Patient 6 Tau2 histogram')
        
      