% This script get the phsic component, labels vectors, estimates Bateman
% parameters and save them along with the original data
% Ahmed Dallal
% Oct 2015

close all, clear all
global gt t
% get root folder
p = uigetdir;
% loop on each patient
for patient = 1:1
    path = strcat(p,{'\patient '},num2str(patient),'\data_analyzed');
    path = path{1};
    cd(path)
    % get number of files
    d = dir('*_scrlist.mat');
    scrList_files = extractfield(d,'name');
    
    % go over each record and do calculations
    for i = 1:length(scrList_files)
        patient_file = strsplit(scrList_files{i},'_scrlist.mat');
        patient_file = strcat(patient_file{1},'.mat');
        load(patient_file)
        load(scrList_files{i});
            [tau_est err labels_vector phasic_Est] = estimate_phasic_Bateman(data,analysis,scrList);
        
        % save the number of scrs with the data
        save(patient_file,'tau_est','err','labels_vector','phasic_Est','-append')
    end
end


% copyrights: Ahmed Dallal