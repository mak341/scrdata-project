clear all;
clc;
folderNames=ls('P*Data');
fileList=cell(size(folderNames,1),1);
for folderIndex=1:size(folderNames,1)
    directoryName=[folderNames(folderIndex,:) '\P' folderNames(folderIndex,end) 'Data\*.csv' ];
    fileList{folderIndex}=ls(directoryName);
    ParticipantId=regexprep(folderNames(folderIndex,:), ' ', '');
    for fileIndex=1:size(fileList{folderIndex},1)
        fileStructNameTemp=fileList{folderIndex}(fileIndex,:);
        tempStrIndx=strfind(fileList{folderIndex}(fileIndex,:),'c');
        if isempty(tempStrIndx)
         tempStrIndx=strfind(fileList{folderIndex}(fileIndex,:),'C');
        end
        fileStructName=fileList{folderIndex}(fileIndex,1:tempStrIndx-2);
        fileStructName=deblank(fileStructName);
        if isempty(strfind(fileStructNameTemp, 'Annotations'))
            fileStructName=regexprep(fileStructName, '-', '_');
            tempIndx=strfind(fileStructName,'_');
            fileStructName=fileStructName(tempIndx(1)+1:end);
            fieldNames={'name', 'date', 'bytes', 'isdir', 'datenum', 'z_axis', 'y_axis', 'x_axis', 'battery', ...
                'temperature', 'EDA', 'Date', 'StartTime', 'SamplingRate', 'time'};
            subfieldName=['Date' fileStructName];
            % Initialize the fields of the struct 
            for fieldIndex=1:length(fieldNames)
              structName=['Data.' ParticipantId '.' subfieldName '.' fieldNames{fieldIndex} '=[]'];
            eval(structName);
            end
            dataDirectoryName=[folderNames(folderIndex,:) '\P' folderNames(folderIndex,end) 'Data\' fileList{folderIndex}(fileIndex,:) ];
            [readData, header, raw]=xlsread(dataDirectoryName);
            Data=setfield(Data, ParticipantId, subfieldName, 'z_axis', readData(:,1));
            Data=setfield(Data, ParticipantId, subfieldName, 'y_axis', readData(:,2));
            Data=setfield(Data, ParticipantId, subfieldName, 'x_axis', readData(:,3));
            Data=setfield(Data, ParticipantId, subfieldName, 'battery', readData(:,4));
            Data=setfield(Data, ParticipantId, subfieldName, 'temperature', readData(:,5));
            Data=setfield(Data, ParticipantId, subfieldName, 'EDA', readData(:,6));
            Data=setfield(Data, ParticipantId, subfieldName, 'SamplingRate', 8);
            Data=setfield(Data, ParticipantId, subfieldName, 'name', fileList{folderIndex}(fileIndex,:));
            tempIndx=strfind(header{6},':');
            tempDate=header{6}(tempIndx(1)+2 : tempIndx(2)-4);
            Data=setfield(Data, ParticipantId, subfieldName, 'Date', tempDate);
            tempHour=str2double(header{6}(tempIndx(2)-2:tempIndx(2)-1));
            tempMin=str2double(header{6}(tempIndx(3)-2:tempIndx(3)-1));
            tempSec=str2double(header{6}(tempIndx(3)+1:tempIndx(3)+2));
            tempStartTimeStr=header{6}(tempIndx(2)-2:tempIndx(3)+2);
            tempStartTime=tempHour*3600+tempMin*60+tempSec;
            tempEndTime=tempStartTime+length(readData(:,1))/8; % Sampling rate is 8 Hz.
            tempTime=tempStartTime:1/8:(tempEndTime-1/8);
            Data=setfield(Data, ParticipantId, subfieldName, 'time', tempTime');
            Data=setfield(Data, ParticipantId, subfieldName, 'StartTime', tempStartTimeStr);
        end
    end
end
save('Data4allPatients.mat', 'Data')