% estimation of bateman equation parameters c, tau_1, tau_2 [x(0) x(1)
% x(2)]
% using the onsets obtained by ledalab
% Ahmed Dallal
% Oct 2015

close all, clear all

global gt t
% load data
load LL_P4Data_Date2011_06_16_analysis.mat
load LL_P4Data_Date2011_06_16_analysis_scrlist.mat

scrLength = 30; % assume 30 second lomg
dT = data.time(2) - data.time(1);
% get the indexes of oncets
[ismem idx] = ismember(scrList.DDA.onset,data.time);
% [ismem idx] = ismember(scrList.DDA.onset,analysis.onset);    % this one
% doesn't work and these two oncet sets doesn't match!
% oncet_index = analysis.onset_idx(idx);
oncet_index = [idx length(data.conductance)+1];   % adding the last index to simplify the loop analysis
% assume the segment extends from current point to the next
for i = 1:length(oncet_index)-1
    % check if time difference is less than the 30
    if data.time(oncet_index(i+1)-1) - data.time(oncet_index(i)) < scrLength
        gt = data.conductance(oncet_index(i):oncet_index(i+1)-1);
        t = dT*[0:length(oncet_index(i):oncet_index(i+1)-1)-1];
    else
        ns = round(scrLength/dT);  % number of samples required for scrLength seconds
        gt = data.conductance(oncet_index(i):oncet_index(i)+ns);
        t = dT*[0:ns];
    end
    
    % optimize parameters ->fmin
    x0 = [1 analysis.tau];
    tau_est(i,:) = fmincon(@bateman_diff, x0, [0 1 -1],[-0.001],[],[],0,Inf); % tau_2 - tau_1 >= 0.001
end

    

% copyrights: Ahmed Dallal 