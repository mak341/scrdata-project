function f = bateman_diff(x)
% calculate f(t,tau), the bateman function, and then get the norm of the
% difference with measurment g(t)
% this function is used for the optimization problem to get an estimate of
% tau_1, tau_2 and the gain c
% x(1) = c, x(2) = tau_1, x(3) = tau_2
% tau_2 - tau_1 >= 0.001
% Ahmed Dallal - Oct 15'

global gt t

ft = x(1) * (exp(-t/x(3)) - exp(-t/x(2))); % micro S .... correctionwith bateman equation
f = norm(ft - gt);