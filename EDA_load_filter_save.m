clc;
%% Load data for Patient
filename = input('What file would you like to load? ','s'); % get file to load
[EDA, time, subID, dateID] = LoadEDAData(filename); 

%% filter filter eda data and save the filtered data as conductance 
load('0p35HzLPFilter.mat')

eda_filt=conv(Num, EDA);
conductance=eda_filt((length(Num)/2)+1: end-length(Num)/2+1); 

%% save file 
event = [];

data = struct('conductance',conductance,'time',time,'event',event);
filename = strcat(subID,'_',dateID,'.mat');

save(filename,'data')