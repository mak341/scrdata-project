function [tau_est err labels_vector phasic_Est] = estimate_phasic_Bateman(data,analysis,scrList)

% estimation of bateman equation parameters c, tau_1, tau_2 [x(0) x(1)
% x(2)]
% using the onsets obtained by ledalab
% Ahmed Dallal
% Oct 2015

global gt t
% load data
% load Ahmed_ORIGINAL_LEDA_Test_1
% load Ahmed_ORIGINAL_LEDA_Test_1_scrlist

scrLength = 30; % assume 30 second long
tau_est = [];
err = [];
phasic_Est = Phasic_estimation(data,scrLength); % estimation of phasic component
dT = data.time(2) - data.time(1);
labels_vector = zeros(size(data.time));
j = 1;  % labeling parameter
% get the indexes of oncets
[ismem idx] = ismember(scrList.DDA.onset,data.time);
% [ismem idx] = ismember(scrList.DDA.onset,analysis.onset);    % this one
% doesn't work and these two oncet sets doesn't match!
% oncet_index = analysis.onset_idx(idx);
oncet_index = [idx length(data.conductance)+1];   % adding the last index to simplify the loop analysis
% assume the segment extends from current point to the next
for i = 1:length(oncet_index)-1
    % check if time difference is less than the 30
    if data.time(oncet_index(i+1)-1) - data.time(oncet_index(i)) < scrLength
        gt = phasic_Est(oncet_index(i):oncet_index(i+1)-1);
        t = dT*[0:length(oncet_index(i):oncet_index(i+1)-1)-1];
        labels_vector(oncet_index(i):oncet_index(i+1)-1) = j;
        j = j + 1;
    else
        ns = round(scrLength/dT);  % number of samples required for scrLength seconds
        gt = phasic_Est(oncet_index(i):oncet_index(i)+ns);
        t = dT*[0:ns];
        labels_vector(oncet_index(i):oncet_index(i)+ns) = j;
        j = j + 1;
    end
    
    % optimize parameters ->fmin
    x0 = [1 analysis.tau];
    [tau_est(i,:) err(i)] = fmincon(@bateman_diff, x0, [0 1 -1],[-0.001],[],[],0,Inf); % tau_2 - tau_1 >= 0.001
    ft = tau_est(i,1) * (exp(-t/tau_est(i,3)) - exp(-t/tau_est(i,2)));
%     plot(t,gt,'b-',t,ft,'r--')
%     pause
%     clf
end

    

% copyrights: Ahmed Dallal 
% last updated: Oct 22, 2015