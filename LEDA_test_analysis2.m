% This script reads patient data, and analyze the remaining two thirds of it for trining using ledalab
% Analyzed data are stored in the "data_analyzed" subfolder for each
% patient
% written by: Ahmed Dallal
% Oct 2015
close all, clear all

% get root folder
p = uigetdir;
% loop on each patient
for patient = 1:3
    %     patient = 1;
    %     path = strcat({'C:\Users\Safaa\Documents\MATLAB\EDA_data\Patient '},num2str(patient));
    path = strcat(p,{'\patient '},num2str(patient))
    path = path{1};
    cd(path)
    % get number of files
    d = dir;
    d([1:3]) = [];  % originally should be [1 2], bbut we have a large file containing all data and we want to omit it
    N_signals = length(d);
    training_samples_I = 1:N_signals;
    load(strcat(path,'\data_analyzed\Training_info.mat'))
    training_samples_I(ismember(training_samples_I,training_samples_ID))=[];    % remove the already analyzed data
    N_training_samples = length(training_samples_I);    % use one third of data as training ... 
        % for each selected signal do analysis
    for i = 1 : N_training_samples
        file_Name = d(training_samples_I(i)).name;
        f = strsplit(file_Name,'.');
        file_Name_A = strcat(f(1),'_analysis','.mat');
        file_Name_A = file_Name_A{1};   % analysis file name
        %% Move -> rename -> analyse -> move back -> delete unwanted
        copyfile(strcat(path,'\',file_Name),strcat(path,'\leda_analysis\',file_Name_A));
        tic;
        % Analyze data
        Ledalab(strcat(path,'\leda_analysis\'), 'open', 'mat','downsample', 8, 'analyze', 'DDA', 'optimize', 1, 'export_scrlist', [0.05, (1)], 'overview',1 )
        leda_t = toc;
        % we may need to count the number of detected SCRs here
        movefile(strcat(path,'\leda_analysis\batchmode_protocol.mat'),strcat(path,'\data_analyzed\protocol_',file_Name_A,'.mat'),'f')
        movefile(strcat(path,'\leda_analysis\*'),strcat(path,'\data_analyzed'),'f')
        cd(strcat(path,'\data_analyzed'))
        save(file_Name_A,'leda_t','-append')
        delete(strcat(path,'\leda_analysis\*'))
        cd(path)
    end
%     cd(strcat(path,'\data_analyzed'))
%     training_data_file_names = extractfield(d(training_samples_ID),'name');
%     save('Training_info.mat','training_data_file_names','training_samples_ID');
end



% Ledalab('D:\Dropbox\UCSDSampleData\DataAnalysis071414\Analysis\', 'open', 'mat','downsample', 2, 'smooth',  {'gauss', 32}, 'analyze', 'DDA', 'optimize', 3, 'export_era', [1 30 .01 1], 'export_scrlist', [0.05, (1)], 'overview',1 )
% 
% 
% Ledalab('D:\Dropbox\UCSDSampleData\Data2Analyze\', 'open', 'leda', 'smooth', {'gauss', 16}, 'analyze', 'DDA', 'optimize', 3, 'overview',1 )


% Ahmed Dallal - Oct 2015