function [ rawEDAdata,Time,subID,dateID,Sf,xa,ya,za,temper ] = LoadEDAData( fileName )
%LoadEDAData Loading rquired data [rawEDAdata,Time]
load(fileName,'Data');
%% Disp and Select Subject
subIDs=fieldnames(Data);
disp('Which subject to label?  ');
for i=1:length(subIDs)
    str2Disp=['(' num2str(i) ')  ' cell2mat(subIDs(i))];
    disp(str2Disp);
end
selectedSubID=input('Please type appropriate subject ID: ');
%% Disp and Select Date
subID=cell2mat(subIDs(selectedSubID));
allData4AllDates=getfield(Data,cell2mat(subIDs(selectedSubID)));
dateIDs=fieldnames(allData4AllDates);
disp('Which date to label?  ');
for i=1:length(dateIDs)
    str2Disp=['(' num2str(i) ')  ' cell2mat(dateIDs(i))];
    disp(str2Disp);
end
selectedDateID=input('Please type appropriate date ID: ');
%% Load Data
dataStruct=getfield(allData4AllDates,cell2mat(dateIDs(selectedDateID)))
dateID=cell2mat(dateIDs(selectedDateID));
Time=dataStruct.time;
rawEDAdata=dataStruct.EDA;
Sf=dataStruct.SamplingRate;
xa=dataStruct.x_axis;
ya=dataStruct.y_axis;
za=dataStruct.z_axis;
temper=dataStruct.temperature;

end

