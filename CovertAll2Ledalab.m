clear all;
clc;
%% Load data for Patient
fileName = input('What file would you like to load? ','s'); % get file to load
load(fileName,'Data');

%% Disp and Select Subject
subIDs=fieldnames(Data);
disp('Which subject to label?  ');
for i=1:length(subIDs)
    str2Disp=['(' num2str(i) ')  ' cell2mat(subIDs(i))];
    disp(str2Disp);
end
selectedSubID=input('Please type appropriate subject ID: ');
%% For each Date filter and save individual mat file
load('0p35HzLPFilter.mat')
subID=cell2mat(subIDs(selectedSubID));
allData4AllDates=getfield(Data,cell2mat(subIDs(selectedSubID)));
dateIDs=fieldnames(allData4AllDates);
for i=1:length(dateIDs)
    %% Load Data
    dataStruct=getfield(allData4AllDates,cell2mat(dateIDs(i)))
    dateID=cell2mat(dateIDs(i));
    Time=dataStruct.time;
    rawEDAdata=dataStruct.EDA;
    Sf=dataStruct.SamplingRate;
    xa=dataStruct.x_axis;
    ya=dataStruct.y_axis;
    za=dataStruct.z_axis;
    temper=dataStruct.temperature;
    
    %Filter data 
    eda_filt=conv(Num, rawEDAdata);
    conductance=eda_filt((length(Num)/2)+1: end-length(Num)/2+1);
    
    %% save file 
    event = [];

    data = struct('conductance',conductance,'time',Time,'event',event);
    filename = strcat('LL_',subID,'_',dateID,'.mat');

    save(filename,'data')
  
end

disp('All files created');